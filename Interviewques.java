import java.util.*;

public class Interviewques {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Integer> arr1 = new ArrayList<>();
        ArrayList<Integer> arr2 = new ArrayList<>();
        Set<Integer> commonElements = new HashSet<>();


        System.out.println("1ci Listin Elementlerini girin:");
        for (String digit : scanner.nextLine().split(" ")) {
            arr1.add(Integer.parseInt(digit));
        }
        System.out.println("2ci Listin Elementlerini girin:");
        for (String digit : scanner.nextLine().split(" ")) {
            arr2.add(Integer.parseInt(digit));
            if (arr2.contains(Integer.parseInt(digit))) {
                commonElements.add(Integer.parseInt(digit));
            }
        }

        System.out.println(commonElements);
    }
}

